#############################################################################################################
# Script to delete all directory contents
#############################################################################################################

param (
	[string]$DIRPATHS
)

#Loop through directory list
foreach($dir in $DIRPATHS){
	if (Test-Path $dir) 
	{
		Get-ChildItem "$dir" -Recurse | Where-Object { $_.Name -ne ".gitignore" } | % { Remove-Item -Path $_.FullName -Force -Recurse}
	}
}
