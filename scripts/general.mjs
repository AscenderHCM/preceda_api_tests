import fs from 'fs';
import { parseString } from 'xml2js'

export const fileExists = async path => !!(await fs.promises.stat(path).catch(e => false));

export const convertXmlToJson = async (xml) => {
    return new Promise((resolve, reject) => {
        parseString(xml, function (err, json) {
            if (err)
                reject(err);
            else
                resolve(json);
        });
    });
};