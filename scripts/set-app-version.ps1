﻿# The script is to read the application versions from the text files and save as azure pipeline variables

$CURR_REL_FILEPATH = "config/curr-release-version.txt"
$FT_REL_FILEPATH = "config/ft-release-version.txt"
$REL_FILEPATH = "config/release-version.txt"

if (Test-Path $CURR_REL_FILEPATH) 
{
    $appVersion = Get-Content -Path $CURR_REL_FILEPATH
    Write-Host "##vso[task.setvariable variable=currReleaseAppVersion]$appVersion"
}

if (Test-Path $FT_REL_FILEPATH) 
{
    $appVersion = Get-Content -Path $FT_REL_FILEPATH
    Write-Host "##vso[task.setvariable variable=ftReleaseAppVersion]$appVersion"
    Write-Host "##vso[task.setvariable variable=ftReleaseFlag]true"
}

if (Test-Path $REL_FILEPATH) 
{
    $appVersion = Get-Content -Path $REL_FILEPATH
    Write-Host "##vso[task.setvariable variable=releaseAppVersion]$appVersion"
}

