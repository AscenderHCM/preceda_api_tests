/*
The script is used to find the correct application version on which tests are run. The version is then attached to CI test results.
This allows any further reporting to utilize and show correct application versions

Command Line Arguments
First Argument - JIRA Username
Second Argument - JIRA Token
Third Argument - Release Type (Values can be - CURRENT-RELEASE or FAST-TRACK)
*/
const axios = require('axios')
const fs = require('fs-extra')
let appVersion = undefined;
let moment = require('moment');
let selectedReleaseDate = undefined;
let username = process.argv[2];
let password = process.argv[3];
let releaseSearchText = process.argv[4];

let requestConfig = {
  auth: {
    'username': username,
    'password': password
  },
}

axios.get('https://ascender.atlassian.net/rest/api/2/project/10113/version?expand=issuesstatus&maxResults=200&orderBy=-sequence&startAt=0&status=unreleased', requestConfig).then((response) => {
  let releaseList = response.data.values;

  //Current release version
  if (releaseSearchText === undefined) {
    for (const releaseItem of releaseList) {

      //Find the JIRA release with a future release date closest to current date
      if (!releaseItem.name.toLowerCase().includes('ft') && releaseItem.releaseDate && moment(releaseItem.releaseDate).format("YYYY-MM-DD") >= moment(new Date()).format("YYYY-MM-DD")) {
        if (selectedReleaseDate === undefined || selectedReleaseDate > moment(releaseItem.releaseDate).format("YYYY-MM-DD")) {
          selectedReleaseDate = moment(releaseItem.releaseDate).format("YYYY-MM-DD");
          console.log(releaseItem.name);
          appVersion = releaseItem.name;
        }
      }
    }
  }
  //Fast track release
  else {
    for (const releaseItem of releaseList) {

      //Find the JIRA release with a future release date closest to current date
      if (releaseItem.name.includes(releaseSearchText) && releaseItem.releaseDate && moment(releaseItem.releaseDate).format("YYYY-MM-DD") >= moment(new Date()).format("YYYY-MM-DD")) {
        if (selectedReleaseDate === undefined || selectedReleaseDate > moment(releaseItem.releaseDate).format("YYYY-MM-DD")) {
          selectedReleaseDate = moment(releaseItem.releaseDate).format("YYYY-MM-DD");
          console.log(releaseItem.name);
          appVersion = releaseItem.name;
        }
      }
    }
  }
  //If a matching release version has been found, write to the config file
  if (appVersion) {
    let appVersionFileName = (releaseSearchText !== undefined) ? `${releaseSearchText.toLowerCase()}-release-version.txt` : 'curr-release-version.txt';
    fs.writeFile(process.cwd() + `/config/${appVersionFileName}`, appVersion, err => {
      if (err) {
        console.error(err)
        return
      }
    })
  }
}, (error) => {
  console.error(error.response)
})


