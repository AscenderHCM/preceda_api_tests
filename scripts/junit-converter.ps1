$XSLFile = $PSScriptRoot + "\junit.xslt"
$ReportDir = "src\access-tests\reports\"
$XMLInputFile = $PSScriptRoot + "\..\" + $ReportDir + "test-result.xml"
$XMLOutputFile = $PSScriptRoot + "\..\" + $ReportDir + "junit-result.xml"

$xslt = New-Object System.Xml.Xsl.XslCompiledTransform;
$xslt.load($XSLFile)
$xslt.Transform($XMLInputFile, $XMLOutputFile)