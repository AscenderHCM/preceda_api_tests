/*
The script is used to find the correct application version on which tests are run, fetched via preceda APIs and JIRA. 
The version is then attached to CI test results. This allows any further reporting to utilize and show correct application versions

Command Line Arguments
First Argument - FileLibrary Name
Second Argument - JIRA Username
Third Argument - JIRA Token
Fourth Argument - Release Type (Allowed Values - 'FT')
*/

import got from 'got'
import fs from 'fs-extra'
import formData from 'form-data'
import { convertXmlToJson } from './general.mjs'

let baseUrl
let loginAPIUrl
let baseInfoAPIUrl

//Check if file library name is provided
if (process.argv[2] === undefined || process.argv[3] === undefined || process.argv[4] === undefined) {
  throw new Error("'Filelibrary name', 'JIRA Username' and 'JIRA password' needs to be passed as arguments")
}

if (process.argv[5] !== undefined && process.argv[5].toUpperCase() !== 'FT') {
  throw new Error("Only 'FT' accepted as release type")
}

let fileLibrary = process.argv[2];
let jiraUsername = process.argv[3];
let jiraPassword = process.argv[4];

//Fetch user credentials for preceda API calls
const credentialsFilePath = process.cwd() + "\\config\\web-credentials.json"
const credentialsFileData = JSON.parse(fs.readFileSync(credentialsFilePath, "utf8"));
let userDetails = credentialsFileData[fileLibrary].userList[0]
let env = credentialsFileData[fileLibrary].env.toLowerCase()

//Determine base url
if (process.env.condepFlag === 'true') {
  baseUrl = `https://10.180.2.218`
}
else {
  baseUrl = `https://${env}.preceda.com.au`
}

loginAPIUrl = `${baseUrl}/logon`


//Form data for login API call
var loginForm = new formData();
loginForm.append('action', 'checkuser');
loginForm.append('user', userDetails.username);
loginForm.append('password', userDetails.password);
loginForm.append('client', fileLibrary);

let loginCallOptions = {
  https: { rejectUnauthorized: false },
  body: loginForm
};
let loginResponse = await got.post(loginAPIUrl, loginCallOptions)
let loginJsonResponse = await convertXmlToJson(loginResponse.body)
//Extract details from login response
let partitionId = loginJsonResponse.response.databases[0].database[0].partition[0]
let precedaPath = loginJsonResponse.response.databases[0].database[0].path[0]
let aspGroup = loginJsonResponse.response.databases[0].database[0].aspgroup[0]

//Form data for baseInfo API call
baseInfoAPIUrl = `${baseUrl}/${precedaPath}/lansaweb?procfun+sys000+pp0006+${partitionId}`
let baseInfoCallOptions = {
  https: { rejectUnauthorized: false },
  form: {
    '_PARTITION': partitionId,
    '_PROCESS': 'SYS000',
    '_FUNCTION': 'PP0006',
    'ALOGNAM': userDetails.username,
    'LUSRPWD': userDetails.password,
    'AACFLIB': fileLibrary,
    'AASPGRP': aspGroup,
    'ACLIENT': fileLibrary,
    'AL_CHKEXP': 'Y'
  }
};

const baseInfoResponse = await got.post(baseInfoAPIUrl, baseInfoCallOptions)
let baseInfoJsonResponse = await convertXmlToJson(baseInfoResponse.body)

//Extract preceda PCR Number
let pcrNum = baseInfoJsonResponse.SCREEN.PCRNO[0]

if (pcrNum === undefined) {
  throw new Error("PCR Number not available")
}


//Form data for JIRA issue API call
let jiraIssueSearchAPIUrl = `https://ascender.atlassian.net/rest/api/3/search`
let jiraIssueSearchCallOptions = {
  https: { rejectUnauthorized: false },
  username: jiraUsername,
  password: jiraPassword,
  searchParams: {
    jql: `project = PREC AND issuetype = Deployment AND resolution in (Unresolved, Corrected, Done, Completed, none, "Un resolved") AND "Release Branch[Short text]" ~ ${pcrNum}`,
    fields: 'fixVersions'
  }
};

const jiraIssueSearchResponse = JSON.parse((await got.get(jiraIssueSearchAPIUrl, jiraIssueSearchCallOptions)).body)
if (jiraIssueSearchResponse.total === 0) {
  throw new Error("No matching JIRA issue of type 'deployment' found")
}
let precVersion = jiraIssueSearchResponse.issues[0].fields.fixVersions[0].name

//Write preceda version to file
let appVersionFileName = (process.argv[5] !== undefined) ? `${process.argv[5].toLowerCase()}-release-version.txt` : 'release-version.txt';
await fs.writeFile(process.cwd() + `/config/${appVersionFileName}`, precVersion);

//Set env variable for preceda version
if (process.argv[5] !== undefined) {
  process.env.PREC_FT_VERSION = precVersion
}
else {
  process.env.PREC_VERSION = precVersion
}