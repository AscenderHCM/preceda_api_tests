<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:template match="/testResults">
	<testsuites>
			<testsuite>
				<xsl:for-each select="*">
					<testcase>
						<xsl:attribute name="classname"><xsl:value-of select="name()"/></xsl:attribute>
						<xsl:attribute name="tn"><xsl:value-of select="@tn"/></xsl:attribute>
						<xsl:attribute name="name"><xsl:value-of select="@lb"/></xsl:attribute>
						<xsl:attribute name="time"><xsl:value-of select="@lt div 1000"/></xsl:attribute>
						<xsl:attribute name="responseData"><xsl:value-of select="*"/></xsl:attribute>
						<xsl:attribute name="rc"><xsl:value-of select="@rc"/></xsl:attribute>
						<xsl:attribute name="rm"><xsl:value-of select="@rm"/></xsl:attribute>
						<xsl:choose>
							<xsl:when test="assertionResult/failureMessage">
								<failure>
									<!-- show only the first failure message (if multiple) as the JUnit schema only supports one faulure node -->
									<xsl:attribute name="message"><xsl:value-of select="assertionResult[./failure = 'true']/failureMessage"/></xsl:attribute>
									<!-- show only the first failure type (if multiple) as the JUnit schema only supports one faulure node -->
									<xsl:attribute name="type"><xsl:value-of select="assertionResult[./failure = 'true']/name"/></xsl:attribute>
								</failure>
							</xsl:when>
							<xsl:when test="@s = 'false'">
								<xsl:choose>
									<xsl:when test="responseData">
										<error><xsl:value-of select="responseData"/></error>
									</xsl:when>
									<xsl:otherwise>
										<failure>true</failure>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
						</xsl:choose>
                        <xsl:if test="@s = 'false'">
                            <xsl:if test="responseData">
                                <error><xsl:value-of select="responseData"/></error>
                            </xsl:if>
                        </xsl:if>
					</testcase>
				</xsl:for-each>
			</testsuite>
		</testsuites>
	</xsl:template>
</xsl:stylesheet>