#############################################################################################################
# Script to delete files if exists
#############################################################################################################

param (
	[string[]]$FILEPATHS
)

#Read each test file and invoke powershell to run tests
foreach($file in $FILEPATHS){
	if (Test-Path $file) 
	{
	Remove-Item $file -Force -Recurse
	}
}
