## Introduction

This repository has the backend permission tests written using JMeter

## More Documentation
More details about the tests can be found [here](https://ascender.atlassian.net/wiki/spaces/Preceda/pages/6442450951/Backend+Permission+Tests)

## Excluded Screens
Below screens are excluded from the tests. They will be re-visited when there is further work around security check on screens.

### NO Access (Tests the screens for which backend security check has been already added)
* M000573,Define Users - *Not returing the expected error message*
* M000825,Self Service Users - *Not returing the expected error message*
* M001764,PeopleStreme Gadget - *Not returing the expected error message*
* M001719,Submit Retro Pay Generation - *Not availble in the master screen list. Need to confirm if the screen is still being used ?*

### HAS Access (Tests the screens for which backend security check has NOT been added)
* M000652,Position - *Returns web app error. May need to pass more params ?*
* M000814,Email Pay slip - *Returns web app error. May need to pass more params ?*
* S000079,Email Pay slip - *Returns web app error. May need to pass more params ?*
* M001619,External Interface to Preceda Mapping Details - *Returns access error. May need some setting on the file library to access this screen?*